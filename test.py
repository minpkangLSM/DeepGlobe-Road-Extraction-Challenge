import torch
import torch.nn as nn
import torch.utils.data as data
from torch.autograd import Variable as V

import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
import pickle

from time import time

from networks.unet import Unet
from networks.dunet import Dunet
from networks.dinknet import LinkNet34, DinkNet34, DinkNet50, DinkNet101, DinkNet34_less_pool

BATCHSIZE_PER_CARD = 2

class TTAFrame():

    def __init__(self, net):
        self.net = net().cuda()
        self.net = torch.nn.DataParallel(self.net, device_ids=range(torch.cuda.device_count()))
        
    def test_one_img_from_path(self, path, evalmode = True):
        if evalmode:
            self.net.eval()
        batchsize = torch.cuda.device_count() * BATCHSIZE_PER_CARD
        print(batchsize)
        if batchsize >= 8:
            return self.test_one_img_from_path_1(path)
        elif batchsize >= 4:
            return self.test_one_img_from_path_2(path)
        elif batchsize >= 2:
            return self.test_one_img_from_path_4(path)

    def test_one_img_from_path_8(self, path):
        img = cv2.imread(path)#.transpose(2,0,1)[None]
        img90 = np.array(np.rot90(img))
        img1 = np.concatenate([img[None],img90[None]])
        img2 = np.array(img1)[:,::-1]
        img3 = np.array(img1)[:,:,::-1]
        img4 = np.array(img2)[:,:,::-1]
        
        img1 = img1.transpose(0,3,1,2)
        img2 = img2.transpose(0,3,1,2)
        img3 = img3.transpose(0,3,1,2)
        img4 = img4.transpose(0,3,1,2)
        
        img1 = V(torch.Tensor(np.array(img1, np.float32)/255.0 * 3.2 -1.6).cuda())
        img2 = V(torch.Tensor(np.array(img2, np.float32)/255.0 * 3.2 -1.6).cuda())
        img3 = V(torch.Tensor(np.array(img3, np.float32)/255.0 * 3.2 -1.6).cuda())
        img4 = V(torch.Tensor(np.array(img4, np.float32)/255.0 * 3.2 -1.6).cuda())
        
        maska = self.net.forward(img1).squeeze().cpu().data.numpy()
        maskb = self.net.forward(img2).squeeze().cpu().data.numpy()
        maskc = self.net.forward(img3).squeeze().cpu().data.numpy()
        maskd = self.net.forward(img4).squeeze().cpu().data.numpy()
        
        mask1 = maska + maskb[:,::-1] + maskc[:,:,::-1] + maskd[:,::-1,::-1]
        mask2 = mask1[0] + np.rot90(mask1[1])[::-1,::-1]
        
        return mask2

    def test_one_img_from_path_4(self, path):

        img = cv2.imread(path)#.transpose(2,0,1)[None]
        print("Original Image shape : ", np.shape(img))
        img90 = np.array(np.rot90(img, axes=(0,1)))
        print("90 rot Imge shape : ", np.shape(img90))
        print("Save img90.")
        cv2.imwrite("img90.jpg", img90)
        print(np.shape(img90))
        img1 = np.concatenate([img[None],img90[None]])
        print("1 img1_shape : ", np.shape(img1))
        img2 = np.array(img1)[:,::-1]
        print("1 img2_shape : ", np.shape(img2))
        img3 = np.array(img1)[:,:,::-1]
        print("1 img3_shape : ", np.shape(img3))
        img4 = np.array(img2)[:,:,::-1]
        print("1 img4_shape : ", np.shape(img4))

        img1 = img1.transpose(0,3,1,2)
        img2 = img2.transpose(0,3,1,2)
        img3 = img3.transpose(0,3,1,2)
        img4 = img4.transpose(0,3,1,2)
        print("2 img1_shape : ", np.shape(img1))
        print("2 img2_shape : ", np.shape(img2))
        print("2 img3_shape : ", np.shape(img3))
        print("2 img4_shape : ", np.shape(img4))

        img1 = V(torch.Tensor(np.array(img1, np.float32)/255.0 * 3.2 -1.6).cuda())
        img2 = V(torch.Tensor(np.array(img2, np.float32)/255.0 * 3.2 -1.6).cuda())
        img3 = V(torch.Tensor(np.array(img3, np.float32)/255.0 * 3.2 -1.6).cuda())
        img4 = V(torch.Tensor(np.array(img4, np.float32)/255.0 * 3.2 -1.6).cuda())

        maska = self.net.forward(img1).squeeze().cpu().data.numpy()
        maskb = self.net.forward(img2).squeeze().cpu().data.numpy()
        maskc = self.net.forward(img3).squeeze().cpu().data.numpy()
        maskd = self.net.forward(img4).squeeze().cpu().data.numpy()

        # #mask1 = maska
        # # print("b : ", np.rot90(maskb[:,::-1],3).shape)
        # # print("b : ", np.rot90(maskc[:,:,::-1],3).shape)
        # # print("b : ", np.rot90(maskd[:,::-1,::-1, 3).shape)

        mask1 = maska + np.rot90(maskc[:,:,::-1], 2, axes=(-2,-1))  + np.rot90(maskd[:], 2, axes=(-2,-1)) + np.flip(maskb,2)# + np.rot90(maskd[:,::-1,::-1], 3, axes=(-2,-1)) # +  np.rot90(maskb[:,::-1], axes=(-2,-1))
        return mask1[0]
    
    def test_one_img_from_path_2(self, path):
        img = cv2.imread(path)#.transpose(2,0,1)[None]
        img90 = np.array(np.rot90(img))
        img1 = np.concatenate([img[None],img90[None]])
        img2 = np.array(img1)[:,::-1]
        img3 = np.concatenate([img1,img2])
        img4 = np.array(img3)[:,:,::-1]
        img5 = img3.transpose(0,3,1,2)
        img5 = np.array(img5, np.float32)/255.0 * 3.2 -1.6
        img5 = V(torch.Tensor(img5).cuda())
        img6 = img4.transpose(0,3,1,2)
        img6 = np.array(img6, np.float32)/255.0 * 3.2 -1.6
        img6 = V(torch.Tensor(img6).cuda())
        
        maska = self.net.forward(img5).squeeze().cpu().data.numpy()#.squeeze(1)
        maskb = self.net.forward(img6).squeeze().cpu().data.numpy()
        
        mask1 = maska + maskb[:,:,::-1]
        mask2 = mask1[:2] + mask1[2:,::-1]
        mask3 = mask2[0] + np.rot90(mask2[1])[::-1,::-1]
        
        return mask3
    
    def test_one_img_from_path_1(self, path):
        img = cv2.imread(path)#.transpose(2,0,1)[None]
        
        img90 = np.array(np.rot90(img))
        img1 = np.concatenate([img[None],img90[None]])
        img2 = np.array(img1)[:,::-1]
        img3 = np.concatenate([img1,img2])
        img4 = np.array(img3)[:,:,::-1]
        img5 = np.concatenate([img3,img4]).transpose(0,3,1,2)
        img5 = np.array(img5, np.float32)/255.0 * 3.2 -1.6
        img5 = V(torch.Tensor(img5).cuda())
        
        mask = self.net.forward(img5).squeeze().cpu().data.numpy()#.squeeze(1)
        mask1 = mask[:4] + mask[4:,:,::-1]
        mask2 = mask1[:2] + mask1[2:,::-1]
        mask3 = mask2[0] + np.rot90(mask2[1])[::-1,::-1]
        
        return mask3

    def load(self, path):
        self.net.load_state_dict(torch.load(path))

if __name__ == "__main__" :

    #source = 'dataset\\valid\\'
    source = 'dataset_all\\test\\'
    val = os.listdir(source)
    solver = TTAFrame(DinkNet34)
    solver.load('weights\\dink34_multi.th')
    tic = time()
    target = 'submits\\dink34_multi\\'
    #os.mkdir(target)

    for i,name in enumerate(val):
        mask_zero = np.zeros([1024, 1024, 3])
        if i%10 == 0:
            print(i/10, '    ','%.2f'%(time()-tic))
        mask = solver.test_one_img_from_path(source+name)
        print("herer : ", mask.shape)
        mask = mask.transpose(1,2,0)
        print("herer : ", mask.shape)
        for i in range(mask.shape[0]) :
            for j in range(mask.shape[1]) :
                num = np.argmax(mask[i][j][:])
                if num == 0 :
                    mask_zero[i][j] = [194, 230, 254]
                elif num == 1 :
                    mask_zero[i][j] = [111, 193, 223]
                elif num == 2 :
                    mask_zero[i][j] = [132, 132, 192]
                elif num == 3 :
                    mask_zero[i][j] = [184, 131, 237]
                elif num == 4 :
                    mask_zero[i][j] = [164, 176, 223]
                elif num == 5 :
                    mask_zero[i][j] = [138, 113, 246]
                elif num == 6 :
                    mask_zero[i][j] = [254, 38, 229]
                elif num == 7 :
                    mask_zero[i][j] = [81, 50, 197]
                elif num == 8 :
                    mask_zero[i][j] = [78, 4, 252]
                elif num == 9 :
                    mask_zero[i][j] = [42, 65, 247]
                elif num == 10 :
                    mask_zero[i][j] = [0, 0, 115]
                elif num == 11 :
                    mask_zero[i][j] = [18, 177, 246]
                elif num == 12 :
                    mask_zero[i][j] = [0, 122, 255]
                elif num == 13 :
                    mask_zero[i][j] = [27, 88, 199]
                elif num == 14 :
                    mask_zero[i][j] = [191, 255, 255]
                elif num == 15 :
                    mask_zero[i][j] = [168, 230, 244]
                elif num == 16 :
                    mask_zero[i][j] = [102, 249, 247]
                elif num == 17 :
                    mask_zero[i][j] = [10, 228, 245]
                elif num == 18 :
                    mask_zero[i][j] = [115, 220, 223]
                elif num == 19 :
                    mask_zero[i][j] = [44, 177, 184]
                elif num == 20 :
                    mask_zero[i][j] = [18, 145, 184]
                elif num == 21:
                    mask_zero[i][j] = [0, 100, 170]
                elif num == 22 :
                    mask_zero[i][j] = [44, 160, 51]
                elif num == 23 :
                    mask_zero[i][j] = [64, 79, 10]
                elif num == 24 :
                    mask_zero[i][j] = [51, 102, 51]
                elif num == 25 :
                    mask_zero[i][j] = [148, 213, 161]
                elif num == 26 :
                    mask_zero[i][j] = [90, 228, 128]
                elif num == 27 :
                    mask_zero[i][j] = [90, 176, 113]
                elif num == 28 :
                    mask_zero[i][j] = [51, 126, 96]
                elif num == 29 :
                    mask_zero[i][j] = [208, 167, 180]
                elif num == 30 :
                    mask_zero[i][j] = [153, 116, 153]
                elif num == 31 :
                    mask_zero[i][j] = [162, 30, 124]
                elif num == 32 :
                    mask_zero[i][j] = [236, 219, 193]
                elif num == 33 :
                    mask_zero[i][j] = [202, 197, 171]
                elif num == 34 :
                    mask_zero[i][j] = [165, 182, 171]
                elif num == 35 :
                    mask_zero[i][j] = [138, 90, 88]
                elif num == 36 :
                    mask_zero[i][j] = [172, 181, 123]
                elif num == 37 :
                    mask_zero[i][j] = [255, 242, 159]
                elif num == 38 :
                    mask_zero[i][j] = [255, 167, 62]
                elif num == 39 :
                    mask_zero[i][j] = [255, 109, 93]
                elif num == 40 :
                    mask_zero[i][j] = [255, 57, 23]
                elif num == 41 :
                    mask_zero[i][j] = [0, 0, 0]
                else :
                    print("Error")

        cv2.imwrite(target + name[:-7] + 'mask.png', mask_zero.astype(np.uint8))

        # mask[mask>4.0] = 255
        # mask[mask<=4.0] = 0
        # mask = np.concatenate([mask[:,:,None],mask[:,:,None],mask[:,:,None]],axis=2)
        # cv2.imwrite(target+name[:-7]+'mask.png',mask.astype(np.uint8))
