import torch
import torch.nn as nn
import torch.utils.data as data
from torch.autograd import Variable as V
from torch.utils.tensorboard import SummaryWriter
import cv2
import os
import numpy as np
from time import time
from networks.unet import Unet
from networks.dunet import Dunet
from networks.dinknet import LinkNet34, DinkNet34, DinkNet50, DinkNet101, DinkNet34_less_pool
from framework import MyFrame
from loss import dice_bce_loss
from data import ImageFolder

if __name__ == "__main__" :

    SHAPE = (1024,1024)
    ROOT = '.\\dataset_all\\train'
    # filer는 함수의 반환값이 참인 값만 리턴해준다.
    imagelist = filter(lambda x: x.find('sat')!=-1, os.listdir(ROOT))
    imagelist = list(imagelist)
    # -8의 의미는, 확장자와 sat을 이름에서 제외하겠다는 의미
    trainlist = map(lambda x: x[:-8], imagelist)
    trainlist = list(trainlist)

    NAME = 'dink34_multi'
    BATCHSIZE_PER_CARD = 1
    solver = MyFrame(DinkNet34, dice_bce_loss, 2e-4)
    batchsize = torch.cuda.device_count() * BATCHSIZE_PER_CARD
    print("Section1 -- end --")
    # Choose mode == "binary" or "multi-class"
    dataset = ImageFolder(trainlist, ROOT, "multi-class")
    data_loader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batchsize,
        shuffle=True,
        num_workers=4)
    print("Section2 -- end --")
    mylog = open('logs/'+NAME+'.log','w')
    tic = time()
    no_optim = 0
    total_epoch = 2
    train_epoch_best_loss = 100.
    print("Section3 -- end --")

    writer = SummaryWriter()
    for epoch in range(1, total_epoch + 1):
        data_loader_iter = iter(data_loader)
        print("Section4 epoch : ", epoch)
        print("data_loader_iter len : ", len(data_loader_iter))
        train_epoch_loss = 0
        iter_num = 0
        for img, mask in data_loader_iter:
            iter_num += 1
            solver.set_input(img, mask)
            train_loss = solver.optimize()
            train_epoch_loss += train_loss
            if iter_num%30 == 0 : print("iter / train_loss : ", iter_num, train_loss)
        train_epoch_loss /= len(data_loader_iter)
        writer.add_scalar('Loss/epoch', train_epoch_loss, epoch)
        print('********')
        print('epoch:',epoch,' time:',int(time()-tic))
        print('train_loss:',train_epoch_loss)
        print('SHAPE:', SHAPE)
        print('********')
        print('epoch:',epoch,' time:',int(time()-tic))
        print('train_loss:', train_epoch_loss)
        print('SHAPE:',SHAPE)

        if train_epoch_loss >= train_epoch_best_loss:
            no_optim += 1
        else:
            no_optim = 0
            train_epoch_best_loss = train_epoch_loss
            solver.save('weights/'+NAME+'.th')
        if no_optim > 6:
            print('early stop at %d epoch', epoch)
            print('early stop at %d epoch', epoch)
            break
        if no_optim > 3:
            if solver.old_lr < 5e-7:
                break
            solver.load('weights/'+NAME+'.th')
            solver.update_lr(5, factor = True, mylog = mylog)
        mylog.flush()

    print('Finish!')
    mylog.close()