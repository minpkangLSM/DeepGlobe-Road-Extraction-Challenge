"""
2020.05.00. ~
Kangmin Park.
This code is for pre- / post- processing dataset of Semantic Segmentation.
"""
import os
os.environ["OPENCV_IO_MAX_IMAGE_PIXELS"] = pow(2,80).__str__()
"""
If size of image data(ex - orthophoto) was too big, cv2 can't handle it. So, modify availability of size,
before declare cv2. 
"""
import numba as nb
from numba import jit, int64, float64
import csv
import math
import cv2
import timeit
import numpy as np
import pandas as pd
import random as rd
import tifffile as tifi
from datetime import datetime
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
# from sklearn.ensemble import IsolationForest
# from sklearn.neighbors import LocalOutlierFactor

"""
2020.06.03.
This class is set of functions for pre-, post-processing of U-Net data in Cultural heritage.
"""
@jit(float64[:,:](float64[:,:], float64[:,:], float64[:,:,:]), nopython=True)
def annot2semlabel_sub(color_label_list, semantic_label_sub, label):

    semantic_label = semantic_label_sub
    for i in range(0, np.shape(label)[0]):
        for j in range(0, np.shape(label)[1]):

            # preparing for pixel to be error
            cls_ = -1

            pixel_val = label[i, j, :]
            for idx, val_list in enumerate(color_label_list):
                if val_list[0] == pixel_val[0] and val_list[1] == pixel_val[1] and val_list[2] == val_list[2]:
                    cls_ = idx + 1

            if cls_ == -1 : print("========== Error : -1 ========== : No values in color_label_list")
            if cls_ == 43 : cls_ = 42

            semantic_label[i, j] = cls_

    return semantic_label

@jit(float64[:,:](float64[:,:], float64[:,:], float64[:,:]), nopython=True)
def pixel_counter(p_img_matrix, t_img_matrix, temp_num_class):

    temp_conf_matrix = temp_num_class

    for i in range(0, np.shape(p_img_matrix)[0]):
        for j in range(0, np.shape(p_img_matrix)[1]):
            row = t_img_matrix[i,j]-1
            col = p_img_matrix[i,j]-1
            temp_conf_matrix[int(row), int(col)] += 1

    return temp_conf_matrix

@jit(nopython=True)
def label2class(label_file, row, col, zeros_np):
    """
    # No. class starts from 1 not 0
    :param label_file: semantic label file instance, not directory
    :param row: size of row
    :param col: size of col
    :param num_cls: class num
    :return:
    """
    x = zeros_np
    for i in range(row):
        for j in range(col):
            # No. class starts from 1 not 0
            x[i, j, int(label_file[i][j])-1] = 1
    return x

@jit(nopython=True)
def union(pred_inst, truth_inst):
    union = np.logical_or(pred_inst, truth_inst)
    return union

@jit(nopython=True)
def intersect(pred_inst, truth_inst):
    inter = np.logical_and(pred_inst, truth_inst)
    return inter

class preprocessing() :

    def __init__(self):
        self.value = None

    def img_chge(self, folder_dir, base_name):
        """
        change name of images in folder into base-name base order
        :return:
        """
        folder_dir = folder_dir
        base_name = base_name

        # extracting image filer list
        img_list = os.listdir(folder_dir)

        # renaming images which are in the target folder
        print("Started to change name based on base-name.")
        for idx, img_dir in enumerate(img_list) :
            abs_dir = os.path.join(folder_dir, img_dir)
            new_name = base_name + "_" + str(idx) + ".jpg"
            dst = folder_dir + "\\" +  new_name
            os.rename(abs_dir, dst)
        print("Finish to change.")

    def img_format(self, folder_dir, format):

        img_list = os.listdir(folder_dir)
        print("Started to change format based on the input format.")
        for img_nm in img_list:
            abs_dir = os.path.join(folder_dir, img_nm)
            basename = os.path.basename(img_nm)
            filename = os.path.splitext(basename)[0] + "." + format
            dst = os.path.join(folder_dir, filename)
            os.rename(abs_dir, dst)
        print("Finish to change.")

    def img_clip(self, folder_dir, width, height, dst_dir):
        """
        clip image into small size you set on "width" and "height". Marginal parts are filled with zero value.
        :param width:
        :param height:
        :param dst_dir: folder to save result images.
        :return:
        """
        folder_dir = folder_dir
        img_list = os.listdir(folder_dir)
        print(img_list)

        for idx, img_dir in enumerate(img_list) :

            print("Processing {0}th image of {1}.".format(str(idx+1), len(img_list)))

            abs_dir = os.path.join(folder_dir, img_dir)
            # flag : -1 = cv2.IMREAD_UNCHANGED
            _, ext = os.path.splitext(abs_dir)
            if ext == ".tif" :
                print("tif")
                img = tifi.imread(abs_dir)
                code = "tif"
            else :
                img = cv2.imread(abs_dir).astype(np.float64)
                code = "cv2"
            img_shp = np.shape(img)
            width_size = img_shp[1]//width+1
            height_size = img_shp[0]//height+1
            print(img_shp)
            # clipping img instace and save
            for i in range(height_size):
                for j in range(width_size):

                    frame = np.zeros([width, height, img_shp[2]])

                    height_front = i * height
                    height_rear = (i+1) * height

                    width_front = j * width
                    width_rear = (j+1) * width

                    if height_rear > img_shp[0] : height_rear = img_shp[0]
                    if width_rear > img_shp[1] : width_rear = img_shp[1]

                    img_part = img[height_front : height_rear, width_front : width_rear, :]
                    frame[0:height_rear-height_front, :width_rear-width_front, :] = img_part

                    file_dst_dir = dst_dir + "\\" + "changwon_we1_{0}_{1}_mask.png".format(str(i), str(j))
                    if code == "tif" :
                        frame = np.flip(frame, 2)
                        cv2.imwrite(file_dst_dir, frame)
                    elif code == "cv2" :
                        cv2.imwrite(file_dst_dir, frame)

        print("Finished to clip and save images at {0}.".format(str(dst_dir)))

    def remove_artifact(self, folder_dir, dst_dir):
        """
        Remove artifact of label images.
        Pixel annotation tool을 이용하여 이진 label 데이터를 생성하면 테두리에 255값이 부여되는 버그가 있는데, 이를 제거하기 위한 코드임.
        :param folder_dir : folder location of label data
        :param dst_dir : folder to save results images.
        :return:
        """
        folder_dir = folder_dir
        img_list = os.listdir(folder_dir)

        for img_nm in img_list :
            abs_dir = os.path.join(folder_dir, img_nm)
            img = cv2.imread(abs_dir, cv2.IMREAD_GRAYSCALE)

            # remove artifact on the four corner vertex.
            if img[1, 1] < 10:
                img[0, 0] = 0
                img[0, 1] = 0
                img[1, 0] = 0
            if img[1, 510] < 10:
                img[0, 511] = 0
                img[0, 510] = 0
                img[1, 511] = 0
            if img[510, 1] < 10:
                img[510, 0] = 0
                img[511, 0] = 0
                img[511, 1] = 0
            if img[510, 510] < 10:
                img[511, 511] = 0
                img[511, 510] = 0
                img[510, 511] = 0

            # remove artifact on the four edge
            for j in list(range(1, 511)):
                if img[1, j] < 10:
                    img[0, j] = 0
                if img[510, j] < 10:
                    img[511, j] = 0

            for k in list(range(1, 511)):
                if img[k, 1] < 10:
                    img[k, 0] = 0
                if img[k, 510] < 10:
                    img[k, 511] = 0

            cv2.imwrite(dst_dir + str(img_nm), img)

    def annot2semlabels(self, annot_folder_dir, color_dict, dst_dir):
        """
        This code is for creating semantic label data for multi-classification of semantic segmentation.
        * What is semantic label? - refer to Evernote [Deep Learning Review]-[U-Net]
        :param annot_folder_dir:
        :param class_num: the number of classes, dtype = integer
        :param color_dict: RGB color information for each class. dtype = dict(list())
                           dshape = { 1 : [255(B), 255(G), 255(R)], 2 : [255(B), 10(G), 10(R)] ....}
                           * opencv 기준 BGR 순서
        :param mod : "save" or "return"
        :return: Grayscale semantic labels map.
        """
        # Make annotation file dir list
        annot_file_dir_list = []
        annot_file_list = os.listdir(annot_folder_dir)
        for annot_file in annot_file_list:
            annot_file_dir = os.path.join(annot_folder_dir, annot_file)
            annot_file_dir_list.append(annot_file_dir)
        count = 0
        for annot_abs_dir in annot_file_dir_list:
            count += 1
            # datatyep : uint8 -> int64, [:,:,:]
            label = cv2.imread(annot_abs_dir).astype(np.float64)
            (y, x, z) = np.shape(label)  # y : height, x : width, z : depth
            # datatype : float64
            semantic_label_sub = np.zeros([y, x]).astype(np.float64)
            print("{0} / {1} processing...".format(str(count), len(annot_file_dir_list)))
            # datatype : float64
            color_label_list = np.array(list(color_dict.values())).astype(np.float64)
            # numba processing
            semantic_label = annot2semlabel_sub(color_label_list, semantic_label_sub, label)

            basename = os.path.basename(annot_abs_dir)
            name = os.path.splitext(basename)[0]
            cv2.imwrite(str(dst_dir) + "\\" + name + "_pred"+ ".png", semantic_label)

    def label2class(self, label_file, row, col, num_cls):
        """
        # No. class starts from 1 not 0
        :param label_file: semantic label file instance, not directory
        :param row: size of row
        :param col: size of col
        :param num_cls: class num
        :return:
        """
        x = np.zeros([row, col, num_cls])
        for i in range(row):
            for j in range(col):
                # No. class starts from 1 not 0
                x[i, j, int(label_file[i][j])-1] = 1
        return x

    def mIoU(self, pred_folder_dir, truth_folder_dir, row, col, num_cls):

        pred_file_list = os.listdir(pred_folder_dir)
        truth_file_list = os.listdir(truth_folder_dir)
        union_list = np.zeros(num_cls)
        inter_list = np.zeros(num_cls)
        count=0

        for pred_file, truth_file in zip(pred_file_list, truth_file_list) :

            count+=1

            pred_x = np.zeros([row, col, num_cls])
            true_x = np.zeros([row, col, num_cls])

            print("count : {0} / {1}".format(count, len(pred_file_list)))

            pred_file_dir = os.path.join(pred_folder_dir, pred_file)
            truth_file_dir = os.path.join(truth_folder_dir, truth_file)

            pred_inst = cv2.imread(pred_file_dir, cv2.IMREAD_UNCHANGED)
            truth_inst = cv2.imread(truth_file_dir, cv2.IMREAD_UNCHANGED)

            pred_class_feature = label2class(pred_inst, row, col, pred_x)
            truth_class_feature = label2class(truth_inst, row, col, true_x)

            u = np.sum(union(pred_class_feature, truth_class_feature), axis=(0,1))
            i = np.sum(intersect(pred_class_feature, truth_class_feature), axis=(0,1))

            union_list += u
            inter_list += i

        union_list += 1e-4
        class_iou = np.divide(inter_list, union_list)*100

        with open("class_iou.csv", 'w', newline='') as f :
            wr = csv.writer(f, quoting=csv.QUOTE_ALL)
            wr.writerow(class_iou)
            wr.writerow(union_list)
            wr.writerow(inter_list)

    class RgbSvm :

        def __init__(self):
            self.value = None

        def img2xml(self, folder_dir, result_file_dir, shuffle, sigma) :
            """
            SVM으로 어노테이션을 생성하기 학습시킬 rgb 레이블 데이터를 생성하는 코드입니다.
            :param folder_dir: 샘플 이미지 별로 레이블링 된 폴더의 위치가 입력되어야 합니다.
            :param dst_dir: 생성된 rgb label의 xml을 저장할 위치를 입력합니다.
            :param shuffle: 파일 순서를 random으로 Shuffle : True or False
            :param sigma: : Noise 제거를 위한 Gaussian filer의 sigma값을 설정합니다.
            :return:
            """

            # make image folder directory shuffle or not
            label_folder_list = os.listdir(folder_dir)
            label_folder_dir_list = []
            images_dir_dict = {}

            for label_folder in label_folder_list :
                label_folder_dir = os.path.join(folder_dir, label_folder)
                sample_img_list = os.listdir(label_folder_dir)

                img_dir_list = []
                if shuffle == True : rd.shuffle(sample_img_list)

                for img_name in sample_img_list :
                    img_dir = os.path.join(label_folder_dir, img_name)
                    img_dir_list.append(img_dir)

                images_dir_dict[str(label_folder)] = img_dir_list

            if math.floor((6*sigma)%2) == 0 : ksize = 6*sigma+1
            else : ksize = math.floor(6*sigma)
            kernel = (ksize, ksize)
            data_set = []

            for set_key, set_img_dir_list in images_dir_dict.items() :
                for img_dir in set_img_dir_list :

                    img_bgr = cv2.imread(str(img_dir)).astype(np.float64)
                    img_shp = np.shape(img_bgr)
                    blurred = cv2.blur(img_bgr, kernel)
                    blurred = np.reshape(blurred, [-1,3])

                    for i in range(np.shape(blurred)[0]):
                        # if outlier[i] == 1:
                        if True:
                            pxl = blurred[i]
                            bgr_set = {"b": pxl[0], "g": pxl[1], "r": pxl[2], "label": int(set_key)}
                            data_set.append(bgr_set)

            if shuffle == True: rd.shuffle(data_set)
            csv_columns = ["b", "g", "r", "label"]
            with open(result_file_dir, 'w', newline='') as csv_file:
                writer = csv.DictWriter(csv_file, fieldnames=csv_columns)
                writer.writeheader()
                for data in data_set:
                    writer.writerow(data)

        def annotation(self, test_dir, train_file_dir, dst_dir, degree, coef0, C):

            # Make abs image file directory (test image)
            img_list = os.listdir(test_dir)
            img_dir_list = []
            for img_name in img_list :
                img_dir = os.path.join(test_dir, img_name)
                img_dir_list.append(img_dir)

            l_dataset = np.array(pd.read_csv(train_file_dir))
            data_x = l_dataset[:,0:3]
            data_y = l_dataset[:,-1]

            polynominal_svm_clf = Pipeline([
                ("scaler", StandardScaler()),
                ("svm_clf", SVC(kernel="poly", degree=degree, coef0=coef0, C=C))
            ])
            print("Started annotation {0} files, using SVM.".format(str(len(img_dir_list))))
            count = 0
            for img_dir in img_dir_list :
                count += 1
                img_bgr = cv2.imread(img_dir).astype(np.float64)
                img_shp = np.shape(img_bgr)
                img_rslt = np.zeros([img_shp[0], img_shp[1]])
                polynominal_svm_clf.fit(data_x, data_y)

                for i in range(img_shp[0]) :
                    for j in range(img_shp[1]) :
                        apart = np.reshape(img_bgr[i,j], [1,-1])
                        predict = polynominal_svm_clf.predict(apart)
                        if predict[0] == 0 : img_rslt[i,j] = 255;
                file_nm = os.path.basename(img_dir)
                print("Processed {0} / {1} : {2}%".format(str(count), str(len(img_dir_list)), str(count/len(img_dir_list))))
                dir = dst_dir + "\\" + str(file_nm)
                cv2.imwrite(dir, img_rslt)

class postprocessing :

    def __init__(self):
        self.value = None

    def merge_results(self, folder_dir, width, height, result_name, result_dst, image_size = 512):
        """
        Code for merging label results of U-Net into an image.
        U-Net 레이블 결과가 저장되어있는 폴더에는 U-net에 필요한 .txt와 .npy 파일이 있음에 유의한다.(U-Net_Git2의 result 폴더)
        :param folder_dir : result label file directory
        :param width : result image width size
        :param height : result image height size
        :param result_name : name
        :param result_dst : destination directory of result image
        :return: None
        """
        img_list = os.listdir(folder_dir)

        # search last file index
        # jpg 파일 말고 .txt, .npy 파일 두 개가 맨 뒤에 있다. 이를 고려하여 마지막 이미지 파일 이름의 list 순서가 정렬되어있을 때 마지막에서 3번째가 된다.
        # 마지막 영상 파일의 이름이 곧 마지막 행렬의 인덱스가 된다.
        last_img = os.path.splitext(img_list[-3])[0]
        img_nm_split = last_img.split("_")
        row = int(img_nm_split[0])
        col = int(img_nm_split[1])
        print(row, col)
        result_array = np.zeros([(row+1)*image_size, (col+1)*image_size])
        for result_img in img_list[:-2] :
            print(result_img)
            img_dir = folder_dir + "\\" + result_img
            result_img = os.path.splitext(result_img)[0]

            img_nm_split = result_img.split("_")
            row = int(img_nm_split[0])
            col = int(img_nm_split[1])

            img = cv2.imread(img_dir, cv2.IMREAD_GRAYSCALE)
            result_array[row*image_size:row*image_size+image_size, col*image_size:col*image_size+image_size] = img

        result_array = result_array[0:int(height), 0:int(width)]
        dir = result_dst + "\\" + result_name
        cv2.imwrite(dir, result_array)

    def count_pixel(self, folder_dir, class_num):

        # pixel count list
        pixel_list = np.zeros(class_num)

        # import semantic labels
        img_list = os.listdir(folder_dir)
        for img_file in img_list :
            img_abs_dir = os.path.join(folder_dir, img_file)
            img = cv2.imread(img_abs_dir, cv2.IMREAD_UNCHANGED)
            print(img_file)
            for cls in range(1, class_num+1) :
                mask = img==cls
                if len(img[mask]) != 0 :
                    pixel_list[cls-1] += len(img[mask])

        with open("pixel_count.csv", 'w', newline='') as f :
            wr = csv.writer(f, quoting=csv.QUOTE_ALL)
            wr.writerow(pixel_list)

    def confusion_matrix(self, p_folder, t_folder, num_class):

        conf_matrix = np.zeros([num_class, num_class])
        conf_matrix_rate = np.zeros([num_class, num_class])
        p_file_list = os.listdir(p_folder)
        t_file_list = os.listdir(t_folder)
        print("Total images : {0}".format(len(p_file_list)))
        count = 0

        for p_file, t_file in zip(p_file_list, t_file_list) :
            count+=1
            print("{0} / {1} processing.".format(count, len(p_file_list)))
            temp_num_class = np.zeros([int(num_class), int(num_class)]).astype(np.float64)

            p_abs_dir = os.path.join(p_folder, p_file)
            t_abs_dir = os.path.join(t_folder, t_file)

            p_img = cv2.imread(p_abs_dir, cv2.IMREAD_UNCHANGED).astype(np.float64)
            t_img = cv2.imread(t_abs_dir, cv2.IMREAD_UNCHANGED).astype(np.float64)

            temp_num_class = pixel_counter(p_img, t_img, temp_num_class)
            conf_matrix += temp_num_class

        sum = np.sum(conf_matrix, 0)
        for i in range(num_class):
            conf_matrix_rate[:,i] = conf_matrix[:,i]/(sum[i]+1e-4)

        plt.matshow(conf_matrix_rate, cmap=plt.cm.gray_r)
        plt.colorbar()
        plt.show()
        # np.savetxt("confusion_matrix_rate.txt", conf_matrix_rate)





if __name__ == "__main__" :

    # # print("Testing part")
    # # Testing for format changing
    # folder_dir = "E:\Data_list\\2020_Landcover\\Data\\Landcover_mask\\img_clip_temp(up1)"
    # instance = preprocessing()
    # instance.img_format(folder_dir, "jpg")

    # # Testing for image clipping of preprocessing
    # folder_dir = "E:\\Data_list\\2020_Landcover\\Data\\Landcover_mask\\mask_temp"
    # width = 1024
    # height = 1024
    # dst_dir = "E:\\Data_list\\2020_Landcover\\Data\\Landcover_mask\\mask_clip_temp(we1)"
    # instance = preprocessing()
    # instance.img_clip(folder_dir = folder_dir, width = width, height = height, dst_dir = dst_dir)

    # # Testing for merge result images of postprocessing
    #folder_dir = "E:\\Data_list\\master_paper\\cultural_heritage\\deep_learning\\U-net_Git2\\results"
    #width = 4664
    #height = 1630
    #result_name = "test.jpg"
    #result_dst = "E:\\Data_list\\master_paper\\cultural_heritage"
    #instance = postprocessing()
    #instance.merge_results(folder_dir = folder_dir, width = width, height = height, result_name = result_name, result_dst = result_dst)

    # # Testing for creating svm rgb dataset and annotation
    # folder_dir = "E:\\Data_list\\master_paper\\cultural_heritage\\cultural_data\\SVM_preprocessing_data\\DL_dset\\moss"
    # instance = preprocessing.RgbSvm()
    # instance.img2xml(folder_dir = folder_dir, result_file_dir = "E:\\Data_list\\master_paper\\cultural_heritage\\cultural_data\\SVM_preprocessing_data\\svm_train_dset.csv", shuffle = True, sigma=2.5)
    # instance.annotation(test_dir = "E:\\Data_list\\master_paper\\cultural_heritage\\cultural_data\\SVM_preprocessing_data\\test_set",
    #                     train_file_dir = "E:\\Data_list\master_paper\\cultural_heritage\\cultural_data\\SVM_preprocessing_data\\svm_train_dset.csv", dst_dir = "E:\\Data_list\\master_paper\\cultural_heritage\\cultural_data\\SVM_preprocessing_data\\test_rlst",
    #                     degree = 5, coef0 = 1, C = 10)

    # # Testing for creating semantic labels
    # folder_dir = "test_results"
    # dst_folder = "test_results_semlabel"
    # # BGR order # class 41 + 2 - background (black(42), white(43)) -> 43은 42로 변환함
    # color_dict = { 1 : [194, 230, 254], 2 : [111, 193, 223], 3 : [132, 132, 192], 4 : [184, 131, 237], 5 : [164, 176, 223],
    #                6 : [138, 113, 246], 7 : [254, 38, 229], 8 : [81, 50, 197], 9 : [78, 4, 252], 10 : [42, 65, 247],
    #                11 : [0, 0, 115], 12 : [18, 177, 246], 13 : [0, 122, 255], 14 : [27, 88, 199], 15 : [191, 255, 255],
    #                16 : [168, 230, 244], 17 : [102, 249, 247], 18 : [10, 228, 245], 19 : [115, 220, 223], 20 : [44, 177, 184],
    #                21 : [18, 145, 184], 22 : [0, 100, 170], 23 : [44, 160, 51], 24 : [64, 79, 10], 25 : [51, 102, 51],
    #                26 : [148, 213, 161], 27 : [90, 228, 128], 28 : [90, 176, 113], 29 : [51, 126, 96], 30 : [208, 167, 180],
    #                31 : [153, 116, 153], 32 : [162, 30, 124], 33 : [236, 219, 193], 34 : [202, 197, 171], 35 : [165, 182, 171],
    #                36 : [138, 90, 88], 37 : [172, 181, 123], 38 : [255, 242, 159], 39 : [255, 167, 62], 40 : [255, 109, 93],
    #                41 : [255, 57, 23], 42 : [0, 0, 0], 43 : [255, 255, 255]}
    #
    # # annot2semlabels(annot_folder_dir=folder_dir, color_dict=color_dict, dst_dir=dst_folder)
    # instance = preprocessing()
    # instance.annot2semlabels(annot_folder_dir=folder_dir, color_dict=color_dict, dst_dir=dst_folder)

    # # Tester
    # folder_dir = "E:\\Data_list\\2020_Landcover\\DlinkNet2\\Semanticlabels\\train_labels"
    # instance = postprocessing()
    # instance.count_pixel(folder_dir, 42)

    # # Testing for confusion matrix
    # t_semlabel = "E:\\Data_list\\2020_Landcover\\DlinkNet2\\test_results_truth"
    # p_semlabel = "E:\Data_list\\2020_Landcover\\DlinkNet2\\test_results_semlabel"
    #
    # instance = postprocessing()
    # instance.confusion_matrix(t_semlabel, p_semlabel, 42)

    # Testing for mIoU
    p_semlabel = "test_results_semlabel"
    t_semlabel = "test_results_truth"

    instance = preprocessing()
    instance.mIoU(p_semlabel, t_semlabel, 1024, 1024, 42)
